﻿namespace CRUDAlunosT2
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ComboBoxTodosAlunos = new System.Windows.Forms.ComboBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.ListBoxTodosOsAlunos = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ListaBoxProcurar = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.ComboBoxTodosAlunos);
            this.groupBox1.Location = new System.Drawing.Point(63, 80);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4);
            this.groupBox1.Size = new System.Drawing.Size(300, 123);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Listagem de Alunos";
            // 
            // ComboBoxTodosAlunos
            // 
            this.ComboBoxTodosAlunos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxTodosAlunos.FormattingEnabled = true;
            this.ComboBoxTodosAlunos.Location = new System.Drawing.Point(7, 61);
            this.ComboBoxTodosAlunos.Name = "ComboBoxTodosAlunos";
            this.ComboBoxTodosAlunos.Size = new System.Drawing.Size(248, 24);
            this.ComboBoxTodosAlunos.TabIndex = 0;
            // 
            // ListBoxTodosOsAlunos
            // 
            this.ListBoxTodosOsAlunos.FormattingEnabled = true;
            this.ListBoxTodosOsAlunos.ItemHeight = 16;
            this.ListBoxTodosOsAlunos.Location = new System.Drawing.Point(50, 210);
            this.ListBoxTodosOsAlunos.Name = "ListBoxTodosOsAlunos";
            this.ListBoxTodosOsAlunos.Size = new System.Drawing.Size(313, 84);
            this.ListBoxTodosOsAlunos.TabIndex = 1;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.ListaBoxProcurar);
            this.groupBox2.Location = new System.Drawing.Point(420, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(256, 237);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Procurar";
            // 
            // ListaBoxProcurar
            // 
            this.ListaBoxProcurar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ListaBoxProcurar.FormattingEnabled = true;
            this.ListaBoxProcurar.Items.AddRange(new object[] {
            "Id Alunos",
            "Apelido"});
            this.ListaBoxProcurar.Location = new System.Drawing.Point(3, 18);
            this.ListaBoxProcurar.Name = "ListaBoxProcurar";
            this.ListaBoxProcurar.Size = new System.Drawing.Size(121, 24);
            this.ListaBoxProcurar.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Image = global::CRUDAlunosT2.Properties.Resources.search;
            this.button1.Location = new System.Drawing.Point(111, 111);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(139, 120);
            this.button1.TabIndex = 3;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 322);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.ListBoxTodosOsAlunos);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "CRUDAlunosT2";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox ComboBoxTodosAlunos;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ListBox ListBoxTodosOsAlunos;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox ListaBoxProcurar;
    }
}

