﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUDAlunosT2.Modelos
{
    public class Aluno
    {
        public int idAluno{ get; set; }
        public int IdAluno { get; internal set; }
        public string PrimeiroNome { get; set; }
        public string Apelido { get; set; }
        public string NomeCompleto
        {
            get
            { 
                return $"{PrimeiroNome} {Apelido}";
            }

        }


        public override string ToString()
        {
            return $" {IdAluno} - {PrimeiroNome} {Apelido}";
        }
    }
}